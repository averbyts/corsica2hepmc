#include <cstring>
#include <deque>
#include <HepMC3/GenParticle.h>
#include <HepMC3/GenVertex.h>
#include <HepMC3/Reader.h>
#include <HepMC3/WriterAscii.h>
using namespace HepMC3;
class ReaderCORSICA : public Reader {
public:

    ReaderCORSICA(const std::string &filename)
        : m_file(filename), m_stream(0), m_isstream(false)
    {
        if ( !m_file.is_open() ) {
            HEPMC3_ERROR("ReaderCORSICA: could not open input file: " << filename)
        }
        set_run_info(std::make_shared<GenRunInfo>());
        run_info()->set_weight_names(std::vector<std::string> {"Default"});
        m_event_number =0;
    }

    ReaderCORSICA(std::istream & stream)
        : m_stream(&stream), m_isstream(true)
    {
        if ( !m_stream->good() ) {
            HEPMC3_ERROR("ReaderCORSICA: could not open input stream ")
        }
        set_run_info(std::make_shared<GenRunInfo>());
        run_info()->set_weight_names(std::vector<std::string> {"Default"});
        m_event_number =0;
    }


    ReaderCORSICA(std::shared_ptr<std::istream> s_stream)
        : m_shared_stream(s_stream), m_stream(s_stream.get()), m_isstream(true)
    {
        if ( !m_stream->good() ) {
            HEPMC3_ERROR("ReaderCORSICA: could not open input stream ")
        }
        set_run_info(std::make_shared<GenRunInfo>());
    }

    ~ReaderCORSICA() {
        if (!m_isstream) close();
    }

    /// @brief skip events
    bool skip(const int)  override
    {
        return false;
    };

    int corsica_to_pdg(const int n) const {
        if (n==6) return 13;
        HEPMC3_ERROR("ReaderCORSICA: unknown CORSICA particle "<< n)
        return 0;
    }

    /// @brief Load all particles from file
    ///
    /// @param[out] evt Event to be filled
    bool read_event(GenEvent& evt)  override
    {
        if ( (!m_file.is_open()) && (!m_isstream) ) return false;

        const size_t       max_buffer_size = 512;
        char               buf[max_buffer_size];

        while (!failed_io()) {
            m_isstream ? m_stream->getline(buf, max_buffer_size) : m_file.getline(buf, max_buffer_size);

            if ( strlen(buf) < 2 ) continue;
            /// Update the format to prevent those checks
            if (buf[0] == ' ') continue;
            if (buf[0] == '-' && buf[1] == '-') continue;
            if (buf[0] != '-' && buf[0] - '0' >9 ) continue;
            int i[1];
            double d[11];
            istringstream iss(buf);
            iss>>i[0]>>d[0]>>d[1]>>d[2]>>d[3]>>d[4]>>d[5]>>d[6]>>d[7]>>d[8]>>d[9]>>d[10];

            m_pv_bank.push_back(std::pair<GenParticlePtr,GenVertexPtr>(
                                    std::make_shared<GenParticle>(FourVector(d[0],d[1],d[2],d[7]),corsica_to_pdg(i[0]),1),
                                    std::make_shared<GenVertex>(FourVector(d[3],d[4],0,d[5]))
                                ));

        }

        std::sort(m_pv_bank.begin(),m_pv_bank.end(),

                  [] (
                      const std::pair<GenParticlePtr,GenVertexPtr>& a,
                      const std::pair<GenParticlePtr,GenVertexPtr>& b
        ) {
            return a.second->position().t()<b.second->position().t();

        }
                 );


        double t =m_pv_bank.front().second->position().t();
        GenEvent* e = nullptr;
        GenVertexPtr v = nullptr;
        for (auto pv: m_pv_bank)
        {
            if (pv.second->position().t()> t + m_interval)
            {   if (e) {
                    m_event_bank.push_back(e);
                    e= nullptr;
                    v= nullptr;
                }
                t= t+m_interval;
            }
            if (!e) {
                m_event_number++;
                e= new GenEvent(Units::GEV,Units::CM);
                e->set_run_info(run_info());
                e->set_event_number(m_event_number);

                GenParticlePtr b1= std::make_shared<GenParticle>(FourVector(0,0,0,0),1,4);
                e->add_beam_particle(b1);
                v = std::make_shared<GenVertex>();
                v->add_particle_in(b1);
                e->add_vertex(v);
                e->weight("Default") = 1.0;
                GenCrossSectionPtr cs = std::make_shared<GenCrossSection>();
                cs->set_cross_section(1.0,1.0);
                e->set_cross_section(cs);
            }
            GenParticlePtr f = std::make_shared<GenParticle>(FourVector(0,0,0,0),pv.first->pdg_id(),3);
            v->add_particle_out(f);
            pv.second->add_particle_in(f);
            pv.second->add_particle_out(pv.first);
            e->add_vertex(pv.second);
            auto pos=pv.second->position();
            const double nanoseconds_to_natural=1;//FIXME
            pv.second->set_position(FourVector(pos.x(),pos.y(),pos.z(),(pos.t()-t)*nanoseconds_to_natural));
        }
        m_pv_bank.clear();
        if (m_event_bank.empty()) return false;
        evt=*(m_event_bank.front());
        m_event_bank.pop_front();
        return true;
    };



    /// @brief Return status of the stream
    bool failed()  override
    {
        return failed_io() && m_event_bank.empty() ;
    };

    bool failed_io()
    {
        return m_isstream ? (bool)m_stream->rdstate() :(bool)m_file.rdstate();
    };


    /// @todo Implicit cast to bool = !failed()?

    /// @brief Close file stream
    void close()  override
    {   if ( !m_file.is_open()) return;
    };
    void set_interval(const double in) {
        m_interval = in;
    };

private:

    std::ifstream m_file; //!< Input file
    std::shared_ptr<std::istream> m_shared_stream;///< For ctor when reading from temp. stream
    std::istream* m_stream; ///< For ctor when reading from stream
    bool m_isstream; ///< toggles usage of m_file or m_stream
    std::vector<std::pair<GenParticlePtr,GenVertexPtr> > m_pv_bank;
    std::deque<GenEvent*> m_event_bank;
    double m_interval;
    int m_event_number;
};
int main( int argc, char** argv)
{
    if (argc<4) return 3;
    ReaderCORSICA inputA(argv[1]);
    inputA.set_interval(atof(argv[3]));
    if(inputA.failed()) return 1;
    WriterAscii       outputA(argv[2]);
    if(outputA.failed()) return 2;
    while( !inputA.failed() )
    {
        GenEvent evt(Units::GEV,Units::CM);
        inputA.read_event(evt);
        if (!outputA.run_info()) evt.set_run_info(inputA.run_info());
        if( inputA.failed() )  {
            printf("End of file reached. Exit.\n");
            break;
        }
        outputA.write_event(evt);
        evt.clear();
    }
    inputA.close();
    outputA.close();
    return 0;
}
